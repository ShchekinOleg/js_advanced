const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const autoprefixer = require('gulp-autoprefixer');
const cssmin = require('gulp-cssmin');
// const rename = require('gulp-rename');
const del = require('delete');
const htmlmin = require('gulp-htmlmin');

gulp.task('clean', function () {
    return del(['dist/'])
});

gulp.task('buildHtml', function () {
    return gulp.src('src/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('./dist'))
});


gulp.task('buildCss', function () {
    return gulp.src('./src/css/*.css')
        .pipe(concat('style.css'))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cssmin())
        // .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('buildJs', function () {
    return gulp.src('src/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('./dist/'))

});

gulp.task('default', gulp.series('clean', gulp.parallel('buildHtml', 'buildJs', 'buildCss')))